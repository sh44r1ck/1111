test: test.o matrix
	g++ -std=c++11 test.cpp matrix.o -o main

matrix: matrix.cpp matrix.h
	g++ -std=c++11 -c matrix.cpp matrix.h

clean:
	rm -rf *.o