#include <iostream>
#include <ostream>
#include "matrix1.h"

Matrix::~Matrix() {//деструктор удалет массив
    if (matrix_ != nullptr) {
        for (int i = 0; i < n_; i++) {
            delete[] matrix_[i];
        }
        delete[] matrix_;
    }
}

Matrix Matrix::operator*=(double b) {// a=a*b умножаем матрицу на число 
    for (int i = 0; i < this->n_; i++) {
        for (int j = 0; j < this->m_; j++) {
            this->matrix_[i][j] = this->matrix_[i][j] * b;
        }
    }
    return *this;
}

Matrix operator*(const Matrix &a, double b) { //a*2
    Matrix new_matrix = Matrix(a);
    new_matrix *= b;
    return new_matrix;
}

Matrix operator*(double b, const Matrix &a) {// 2*a
    return a * b;
}

//Matrix Matrix::operator+=(const Matrix& b) {
//    if (a.x != b.x || a.n_ != b.n_) {
//        cout << "ОШИБКА РАЗМЕРНОСТИ" << endl;
//        return a;
//    }
//    for (int i = 0; i < a.x; i++) {
//        for (int j = 0; j < a.n_; j++) {
//            a.Matrix_[i][j] += b.Matrix_[i][j];
//        }
//    }
//    return a;
//}

//Matrix operator+(Matrix b) {
//    if (a.n_ != b.x || a.n_ != b.n_) {
//        cout << "ОШИБКА РАЗМЕРНОСТИ" << endl;
//        return a;
//    }
//    for (int i = 0; i < a.x; i++) {
//        for (int j = 0; j < a.n_; j++) {
//            a.Matrix_[i][j] += b.Matrix_[i][j];
//        }
//    }
//    return a;
//}


//Matrix operator-(Matrix a, Matrix b) {
//    if (a.x != b.x || a.n_ != b.n_) {
//        cout << "ОШИБКА РАЗМЕРНОСТИ" << endl;
//        return a;
//    }
//    for (int i = 0; i < a.x; i++) {
//        for (int j = 0; j < a.n_; j++) {
//            a.Matrix_[i][j] -= b.Matrix_[i][j];
//        }
//    }
//    return a;
//}


//Matrix operator-=(Matrix a, Matrix b) {
//    if (a.x != b.x || a.n_ != b.n_) {
//        cout << "ОШИБКА РАЗМЕРНОСТИ" << endl;
//        return a;
//    }
//    for (int i = 0; i < a.x; i++) {
//        for (int j = 0; j < a.n_; j++) {
//            a.Matrix_[i][j] -= b.Matrix_[i][j];
//        }
//    }
//    return a;
//}


Matrix Matrix::operator-() {//b = -a
    Matrix new_matrix = Matrix(*this);
    new_matrix.n_ = n_;
    new_matrix.m_ = m_;
    for (int i = 0; i < n_; i++) {
        for (int j = 0; j < m_; j++) {
            new_matrix.matrix_[i][j] = -matrix_[i][j];
        }
    }

    return new_matrix;
}


std::ostream &operator<<(std::ostream &os, const Matrix &a) {//вывод на экран os поток вывода
    for (int i = 0; i < a.n_; i++) {
        for (int j = 0; j < a.m_; j++) {
            std::cout << a.matrix_[i][j] << ' ';
        }
        std::cout << std::endl;
    }
    return os;
}


Matrix Matrix::identity(int m) {//возвращает единичнуб матрицу
    Matrix a(m, m);
    for (int i = 0; i < m; i++) {
        a.matrix_[i][i] = 1;
    }

    return a;
}


Matrix Matrix::diagonal(const double *vals, int m) {
    Matrix a(m, m);
    for (int i = 0; i < m; i++) {
        a.matrix_[i][i] = vals[i];
    }

    return a;
}


int Matrix::rows() const {//кол строк
    return n_;
}


int Matrix::columns() const {// кол столбцов
    return m_;
}


void Matrix::set(int n, int m, double val) const {//записываем элемент в 
    if (n < n_ && m < m_) {
        matrix_[n][m] = val;
    } else {
        throw MatrixException("ошибка доступа к полю матрицы");
    }
}

