#include <iostream>
#include "matrix.h"

void test_constructors(char *s = nullptr, int m = 2, int n = 3, int l = 1) {
    static double nums[] = {1, 2, 3, 4};
    s = "{{1, 0, 0}, {0, 1, 0.5}}";
    std::cerr << "Matrix constructors" << std::endl;
    std::cerr << "Matrix nxm, n = 3, m = 2" << std::endl;
    Matrix a(n, m);
    std::cerr << a;

    std::cerr << "Matrix 1x1" << std::endl;
    Matrix b(l);
    std::cerr << b;

    std::cerr << "Matrix 1xm and nx1" << std::endl;
    Matrix c(nums, 4);
    Matrix d(4, nums);
    std::cerr << c;
    std::cerr << d;

    std::cerr << "Matrix from string {{1, 0, 0}, {0, 1, 0.5}}" << std::endl;
    Matrix k(s);
    std::cerr << k;
}

void test6(int n = 5) {
    std::cerr << "Matrix identity, n = " << n << std::endl;
    Matrix a = Matrix::identity(n);
    std::cerr << a;
}

void test7(double *nums) {
    if (!nums) {
        double test_nums[] = {1, 2, 3};
        nums = test_nums;
    }

    std::cerr << "Matrix diagonal " << std::endl;
    Matrix a = Matrix::diagonal(nums, 3);;
    std::cerr << a;
}

void test8(char *s) {
    if (s == nullptr) {
        s = "{{1, 0, 0}, {0, 1, 0.5}}";
    }
    Matrix a(s);
    a.rows();
    std::cerr << "Matrix \n" << a << "has " << a.rows() << " rows" << std::endl;
}

void test9(char *s) {
    if (s == nullptr) {
        s = "{{1, 0, 0}, {0, 1, 0.5}}";
    }
    Matrix a(s);
    a.rows();
    std::cerr << "Matrix \n" << a << "has " << a.columns() << " columns" << std::endl;
}

void test10(char *s, int n = 1, int m = 2, double value = 100) {
    if (s == nullptr) {
        s = "{{1, 0, 0}, {0, 1, 0.5}}";
    } else {
        std::cerr << "Введите индексы элемента i, j:" << std::endl;
        std::cin >> n >> m;
        std::cerr << "Введите значение:" << std::endl;
        std::cin >> value;
    }
    Matrix a(s);
    std::cerr << "Matrix \n" << a << std::endl;

    a.set(n, m, value);
    std::cerr << "set a[" << n << "][" << m << "] = " << value << "\n" << a << std::endl;
}

void test11(char *s, int n = 1) {
    if (s == nullptr) {
        s = "{{1, 0, 0}, {0, 1, 0.5}}";
    } else {
        std::cerr << "Введите индекс элемента i:" << std::endl;
        std::cin >> n;
    }
    Matrix a(s);
    std::cerr << "Matrix \n" << a << std::endl;

    std::cerr << "a[" << n << "]\n" << a[n] << std::endl;
}

bool test12(char *s, int n = 1, int m = 2) {
    if (s == nullptr) {
        s = "{{1, 0, 0}, {0, 1, 0.5}}";
    } else {
        std::cerr << "Введите индекс элемента i, j:" << std::endl;
        std::cin >> n >> m;
    }
    Matrix a(s);
    std::cerr << "Matrix \n" << a << std::endl;
    std::cerr << "a[" << n << "][" << m << "]\n" << a[n][m] << std::endl;
}

bool test13(char *s, int n = 9) {
    if (s == nullptr) {
        s = "{{1, 0, 0}, {0, 1, 0.5}}";
    } else {
        std::cerr << "Введите множитель:" << std::endl;
        std::cin >> n;
    }
    Matrix a(s);
    std::cerr << "Matrix \n" << a << std::endl;
    std::cerr << "Matrix a * " << n << "\n" << a * n << std::endl;
    a *= n;
    std::cerr << "Matrix a *= " << n << "\n" << a << std::endl;
}

bool test15(char *s) {
    Matrix b;
    Matrix a;
    if (s == nullptr) {
        s = "{{1, 0, 0}, {0, 1, 0.5}}";
        a = Matrix(s);
        b = a;
    } else {
        a = Matrix(s);
        std::cout << "Введите матрицу:" << std::endl;
        std::cin.getline(s, 1000);
        b = Matrix(s);
    }
    std::cerr << "Matrix a, b\n" << a << "\n" << b << std::endl;
    std::cerr << "Matrix a + b \n" << a + b << std::endl;
    a += b;
    std::cerr << "Matrix a += b\n" << a << std::endl;
}

bool test17(char *s) {
    Matrix b;
    Matrix a;
    if (s == nullptr) {
        s = "{{1, 0, 0}, {0, 1, 0.5}}";
        a = Matrix(s);
        b = a;
    } else {
        a = Matrix(s);
        std::cout << "Введите матрицу:" << std::endl;
        std::cin.getline(s, 1000);
        b = Matrix(s);
    }
    std::cerr << "Matrix a, b \n" << a << "\n" << b << std::endl;
    std::cerr << "Matrix a - b \n" << a - b << std::endl;
    a -= b;
    std::cerr << "Matrix a -= b\n" << a << std::endl;
}

void test(int test_number, char *s = nullptr) {
    std::cerr << "----------------" << std::endl;
    std::cerr << "TEST " << test_number << std::endl;
    try {
        switch (test_number) {
            case 1:
                test_constructors();
                break;
            case 6:
                test6();
                break;
            case 7:
                test7(nullptr);
                break;
            case 8:
                test8(s);
                break;
            case 9:
                test9(s);
                break;
            case 10:
                test10(s);
                break;
            case 11:
                test11(s);
                break;
            case 12:
                test12(s);
                break;
            case 13:
                test13(s);
                break;
            case 15:
                test15(s);
                break;
            case 17:
                test17(s);
                break;
            default:
                std::cerr << "no test with number" << test_number << std::endl;
        }
    } catch (Matrix::MatrixException &exception) {
        std::cerr << "Matrix exception " << exception.get_error() << std::endl;
        std::cerr << "FAILED" << std::endl;
    } catch (std::exception &exception) {
        std::cerr << "Some other exception (" << exception.what() << ")\n";
        std::cerr << "FAILED" << std::endl;
    }
}

int main(int argc, char *argv[]) {
    if (argc == 1) {
        for (int i = 1; i < 18; ++i) {
            test(i, nullptr);
        }
    } else {
        char s[1000];
        std::cerr << "Введите матрицу: " << std::endl;
        std::cin.getline(s, 1000);
        for (int i = 1; i < argc; ++i) {
            int test_number;
            sscanf(argv[i], "%d", &test_number);
            test(test_number, s);
        }
    }
}