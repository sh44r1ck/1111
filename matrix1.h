#include <ostream>
#include <cstring>
#include <cstdlib>

class Matrix {
public:
    int m_;//столбцов
    int n_;//строк
    double **matrix_;

    Matrix() = default;

    ~Matrix();

    Matrix(Matrix const &a) {//конструктор копирования
        n_ = a.n_;
        m_ = a.m_;
        matrix_ = new double *[n_];// соаздем массив из н строк
        for (int i = 0; i < n_; ++i) {
            matrix_[i] = new double[m_];
            for (int j = 0; j < m_; ++j) {
                matrix_[i][j] = a.matrix_[i][j];
            }
        }
    }

    Matrix(double i) {//конструктор делает матрицу 1*1 
        m_ = 1;
        n_ = 1;
        matrix_ = new double *[n_];
        matrix_[0] = new double[m_];
        matrix_[0][0] = i;
    }

    Matrix(int n, int m) {//создаем матрицу
        n_ = n;
        m_ = m;
        matrix_ = new double *[n_];
        for (int i = 0; i < n_; i++) {
            matrix_[i] = new double[m_];
            for (int j = 0; j < m_; ++j) {
                matrix_[i][j] = 0;
            }
        }
    }

    Matrix(const double *nums, int m) {//матрица строка
        n_ = 1;
        m_ = m;
        matrix_ = new double *[n_];
        matrix_[0] = new double[m_];
        for (int i = 0; i < m_; i++) {
            matrix_[0][i] = nums[i];
        }
    }

    Matrix(int n, const double *nums) {//матрица столбец
        n_ = n;
        m_ = 1;
        matrix_ = new double *[n_];
        for (int i = 0; i < n_; i++) {
            matrix_[i] = new double[m_];
            matrix_[i][0] = nums[i];
        }
    }

    explicit Matrix(char *s) {// строковое представление
        char *s_tmp = new char[strlen(s)];//копируем строку
        strcpy(s_tmp, s);

        n_ = -1;//кол строк
        int points = 0;

        for (char i = 0; s[i] != '\0'; i++) {
            if ((s[i] < '0' || s[i] > '9') && s[i] != '.') {
                s_tmp[i] = ' ';
            }
            if (s[i] == '{') {
                n_++;
            } else if (s[i] == ',') {
                points++;
            }
        }

        if (n_ <= 0) {
            throw MatrixException("wrong input");
        }

        m_ = ((points - n_ + 1) / n_) + 1;// кол элементов в строке
        matrix_ = new double *[n_];// массив из н строк
        char *current = nullptr;
        char *next = s_tmp;
        for (int i = 0; i < n_; ++i) {
            matrix_[i] = new double[m_];
            for (int j = 0; j < m_; ++j) {
                char *tmp = current;
                current = next;
                next = tmp;
                matrix_[i][j] = strtod(current, &next);// считаем число из строки и нехт указ на след
                //в current то откуда надо считывать
            }
        }

        delete[] s_tmp;
    }

//    Matrix operator[](int i) {
//        if (i >= 0 && i < m_) {
//            static Matrix fir(1, n_);
//            for (int j = 0; j < n_; j++) {
//                fir.set(0, j, matrix_[i][j]);
//            }
//            return fir;
//        } else if (i >= 0 && i < n_) {
//            static Matrix fir;
//            fir.Matrix(1, m_);
//            for (int j = 0; j < m_; j++) {
//                fir.set(j, 0, matrix_[j][i]);
//            }
//            return fir;
//        } else {
//            cout << "index error" << endl;
//            return *this;
//        }
//    }

    Matrix operator[](int i) {// 11 and 12
        static int flag = 0;
        if (!flag && i < n_) {//строку
            Matrix new_matrix(matrix_[i], m_);
            flag = 1;
            return new_matrix;
        } else if (flag && i < m_) {// 
            Matrix new_matrix(matrix_[0][i]);
            flag = 0;
            return new_matrix;
        } else if (!flag && i < m_) {
            double *col = new double[n_];
            for (int j = 0; j < n_; ++i) {
                col[j] = matrix_[j][i];
            }
            Matrix new_matrix(n_, col);
            return new_matrix;
        }

        throw MatrixException("index error");
    }


//    void out() {
//        for (int i = 0; i < m_; i++) {
//            for (int j = 0; j < n_; j++) {
//                cout << matrix_[i][j] << ' ';
//            }
//            cout << endl;
//        }
//        cout << endl;
//    }


//    void Matrix(const double *st, int m) {
//        x = 1;
//        y = m;
//        mat = new double *[x];
//        for (int i = 0; i < x; i++) {
//            mat[i] = new double[y];
//        }
//        for (int j = 0; j < m; j++) {
//            mat[0][j] = st[j];
//        }
//    }
//
//
//    void Matrix(int m, const double *st) {
//        x = m;
//        y = 1;
//        mat = new double *[x];
//        for (int i = 0; i < x; i++) {
//            mat[i] = new double[y];
//        }
//        for (int i = 0; i < m; i++) {
//            mat[i][0] = st[i];
//        }
//    }


//~matrix_(void);
    static Matrix

    identity(int);

    static Matrix diagonal(const double *, int);

    int rows() const;

    int columns() const;

    void set(int, int, double) const;

    friend std::ostream &operator<<(std::ostream &os, const Matrix &a);

    Matrix operator-();

//    Matrix operator-=(Matrix b);

    friend Matrix operator*(const Matrix &a, double b);

    friend Matrix operator*(double b, const Matrix &a);


    Matrix operator*=(double b);

//    Matrix operator+(Matrix b);
//
//    Matrix operator+=(const Matrix& b);
//
//    Matrix operator-(Matrix b);
    Matrix &operator=(const Matrix &a) {//Присваивание б=а this это б
        if (this == &a) {
            return *this;
        }

        if (matrix_ != nullptr) {
            for (int i = 0; i < n_; i++) {
                delete[] matrix_[i];
            }
            delete[] matrix_;
        }

        n_ = a.n_;
        m_ = a.m_;
        matrix_ = new double *[n_];
        for (int i = 0; i < n_; ++i) {
            matrix_[i] = new double[m_];
            for (int j = 0; j < m_; ++j) {
                matrix_[i][j] = a.matrix_[i][j];
            }
        }

        return *this;
    }

    class MatrixException {//класс для обработки исключений
    public:
        char *m_error;

        explicit MatrixException(char *error) : m_error(error) {}

        static char *get_error(const MatrixException &exception) {
            return exception.m_error;
        }

        ~MatrixException() noexcept = default;
    };

};