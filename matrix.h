#include <cstring>
#include <cstdlib>
#include <ostream>

class Matrix;

class MatrixParent {
public:
    int n_;
    int m_;

    double **matrix_;

    MatrixParent() {
        n_ = 0;
        m_ = 0;
        matrix_ = nullptr;
    }

    ~MatrixParent();

    MatrixParent(MatrixParent const &a);

    MatrixParent(double i);

    MatrixParent(int n, int m);

    MatrixParent(const double *nums, int m);

    MatrixParent(int n, const double *nums);

    MatrixParent(char *s);

    static MatrixParent identity(int);

    static MatrixParent diagonal(const double *, int);

    int rows() const;

    int columns() const;

    void set(int, int, double) const;

    friend std::ostream &operator<<(std::ostream &os, const MatrixParent &a);

    MatrixParent operator-();

    friend MatrixParent operator*(const MatrixParent &a, double b);

    friend MatrixParent operator*(double b, const MatrixParent &a);

    MatrixParent operator*=(double b);

    MatrixParent operator+(const MatrixParent &b);

    MatrixParent &operator+=(const MatrixParent &b);

    MatrixParent operator-(const MatrixParent &b);

    MatrixParent &operator-=(const MatrixParent &b);

    MatrixParent &operator=(const MatrixParent &a) {
        if (this == &a) {
            return *this;
        }

        if (matrix_ != nullptr) {
            for (int i = 0; i < n_; i++) {
                delete[] matrix_[i];
            }
            delete[] matrix_;
        }

        n_ = a.n_;
        m_ = a.m_;
        matrix_ = new double *[n_];
        for (int i = 0; i < n_; ++i) {
            matrix_[i] = new double[m_];
            for (int j = 0; j < m_; ++j) {
                matrix_[i][j] = a.matrix_[i][j];
            }
        }

        return *this;
    }

    class MatrixException : public std::exception {
    public:
        char *m_error;

        explicit MatrixException(char *error) : m_error(error) {}

        char *get_error() {
            return m_error;
        }
    };
};

class MatrixRow : public MatrixParent {
public:
    MatrixParent operator[](int i) {
        if (i < m_) {
            MatrixParent new_matrix(matrix_[0][i]);
            return new_matrix;
        }
        throw MatrixException("index error []2");
    }

    explicit MatrixRow(const MatrixParent &m): MatrixParent(m) {}
};

class Matrix : public MatrixParent {
public:
    Matrix() : MatrixParent() {}

    Matrix(MatrixParent const &a) : MatrixParent(a) {}

    Matrix(double i) : MatrixParent(i) {}

    Matrix(int n, int m) : MatrixParent(n, m) {}

    Matrix(const double *nums, int m) : MatrixParent(nums, m) {}

    Matrix(int n, const double *nums) : MatrixParent(n, nums) {}

    Matrix(char *s) : MatrixParent(s) {}

    MatrixRow operator[](int i) {
        if (i < n_) {
            MatrixParent new_matrix(matrix_[i], m_);
            MatrixRow result(new_matrix);

            return result;
        }
        throw MatrixException("index error []1");
    }
};


//a[1]
//a[1]