#include <iostream>
#include "matrix.h"

using namespace std;

int main(){
    cout << "я написал деструктор, но он почему то работет не на всех тестах. ошибку я найти не смог. не могли бы вы глянуть? 6 строка в matrix_.cpp" << endl;
    cout << endl;
    Matrix first{};
    int var = 0;
    while (var != 99){
        cout << "выберите тест (1-4, 6-13, 15 - 18, 21; \n"
                "99 для выхода; \n"
                "задания 15 и далее в примерах работают одной матрицей(типа а+а), \n"
                "но подходят для любых двух, вкл. проверку размерности" << endl;
        int var;
        cin >> var;
        if(var == 1){
            cout << "введите размерность" << endl;
            int m, n;
            cin >> m;
            cin >> n;
            first.matrix_(m, n);
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 1 И 14" << endl;
            cout << endl;
        }
        else if(var == 2){
            cout << "введите данные" << endl;
            int m;
            cin >> m;
            first.matrix_(m);
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 2" << endl;
            cout << endl;
        }
        else if(var == 3){
            cout << "введите размерность и данные" << endl;
            int m;
            cin >> m;
            double * st;
            st = new double [m];
            for (int i = 0; i < m; i++){
                cin >> st[i];
            }
            first.matrix_(st, m);
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 3" << endl;
            cout << endl;
        }
        else if(var == 4){
            cout << "введите размерность и данные" << endl;
            int m;
            cin >> m;
            double * st;
            st = new double [m];
            for (int i = 0; i < m; i++){
                cin >> st[i];
            }
            first.matrix_(m, st);
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 4" << endl;
            cout << endl;
        }
        else if(var == 6){
            int m;
            cout << "введите размерность" << endl;
            cin >> m;
            first = matrix::identity(m);
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 6" << endl;
            cout << endl;
        }
        else if(var == 7){
            cout << "введите размерность и данные" << endl;
            int m;
            cin >> m;
            double * st;
            st = new double [m];
            for (int i = 0; i < m; i++){
                cin >> st[i];
            }
            first = matrix::diagonal(st, m);
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 7" << endl;
            cout << endl;
        }
        else if(var == 8){
            int m;
            m = first.rows();
            cout << m << endl;
            cout << endl;
            cout << "ПРОРАБОТАЛИ 8" << endl;
            cout << m << endl;
        }
        else if(var == 9){
            int m;
            m = first.collumns();
            cout << m << endl;
            cout << endl;
            cout << "ПРОРАБОТАЛИ 9" << endl;
        }
        else if(var == 10){
            cout << "введите поля и данные" << endl;
            int a, b;
            double c;
            cin >> a;
            cin >> b;
            cin >> c;
            first.set(a, b, c);
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 10" << endl;
            cout << endl;
        }
        else if(var == 11 || var == 12){
            cout << "введите данные" << endl;
            int m;
            cin >> m;
            first = first[m];
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 11, 12" << endl;
        }
        else if(var == 13){
            cout << "введите размерность и данные" << endl;
            int m;
            cin >> m;
            first = first * m;
            first *= m;
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 13 дважды" << endl;
        }
        else if(var == 15){
            first = first + first;
            cout << endl;
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 15" << endl;
        }
        else if(var == 21){
            first = -first;
            cout << endl;
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 21" << endl;
        }
        else if(var == 17){
            first = first - first;
            cout << endl;
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 17" << endl;
        }
        else if(var == 18){
            first -= first;
            cout << endl;
            cout << first << endl;
            cout << "ПРОРАБОТАЛИ 17" << endl;
        }
        else if(var == 99){
            return 0;
        }
    }
}