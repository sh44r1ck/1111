#include <ostream>
#include "matrix.h"

MatrixParent::~MatrixParent() {
    for (int i = 0; i < n_; i++) {
        delete[] matrix_[i];
    }
    delete[] matrix_;
}

MatrixParent::MatrixParent(MatrixParent const &a) {
    n_ = a.n_;
    m_ = a.m_;
    matrix_ = new double *[n_];
    for (int i = 0; i < n_; ++i) {
        matrix_[i] = new double[m_];
        for (int j = 0; j < m_; ++j) {
            matrix_[i][j] = a.matrix_[i][j];
        }
    }
}

MatrixParent::MatrixParent(double i) {
    m_ = 1;
    n_ = 1;
    matrix_ = new double *[n_];
    matrix_[0] = new double[m_];
    matrix_[0][0] = i;
}

MatrixParent::MatrixParent(int n, int m) {
    n_ = n;
    m_ = m;
    matrix_ = new double *[n_];
    for (int i = 0; i < n_; i++) {
        matrix_[i] = new double[m_];
        for (int j = 0; j < m_; ++j) {
            matrix_[i][j] = 0;
        }
    }
}

MatrixParent::MatrixParent(const double *nums, int m) {
    n_ = 1;
    m_ = m;
    matrix_ = new double *[n_];
    matrix_[0] = new double[m_];
    for (int i = 0; i < m_; i++) {
        matrix_[0][i] = nums[i];
    }
}

MatrixParent::MatrixParent(int n, const double *nums) {
    n_ = n;
    m_ = 1;
    matrix_ = new double *[n_];
    for (int i = 0; i < n_; i++) {
        matrix_[i] = new double[m_];
        matrix_[i][0] = nums[i];
    }
}

MatrixParent::MatrixParent(char *s) {
    char *s_tmp = new char[strlen(s) + 1];
    strcpy(s_tmp, s);

    n_ = -1;
    int points = 0;

    for (char i = 0; s[i] != '\0'; i++) {
        if ((s[i] < '0' || s[i] > '9') && s[i] != '.') {
            s_tmp[i] = ' ';
        }
        if (s[i] == '{') {
            n_++;
        } else if (s[i] == ',') {
            points++;
        }
    }

    if (n_ <= 0) {
        throw MatrixException("wrong input");
    }

    m_ = ((points - (n_ - 1)) / n_) + 1;

    if (m_ <= 0) {
        throw MatrixException("wrong input");
    }

    matrix_ = new double *[n_];
    char *current = nullptr;
    char *next = s_tmp;
    for (int i = 0; i < n_; ++i) {
        matrix_[i] = new double[m_];
        for (int j = 0; j < m_; ++j) {
            char *tmp = current;
            current = next;
            next = tmp;
            matrix_[i][j] = strtod(current, &next);
        }
    }

    next = nullptr;
    delete[] s_tmp;
}

MatrixParent MatrixParent::operator*=(double b) {
    for (int i = 0; i < this->n_; i++) {
        for (int j = 0; j < this->m_; j++) {
            this->matrix_[i][j] = this->matrix_[i][j] * b;
        }
    }
    return *this;
}

MatrixParent operator*(const MatrixParent &a, double b) {
    MatrixParent new_matrix = MatrixParent(a);
    new_matrix *= b;
    return new_matrix;
}

MatrixParent operator*(double b, const MatrixParent &a) {
    return a * b;
}

MatrixParent &MatrixParent::operator+=(const MatrixParent &b) {
    if (m_ != b.m_ || n_ != b.n_) {
        throw MatrixException("ОШИБКА РАЗМЕРНОСТИ");
    }
    for (int i = 0; i < n_; i++) {
        for (int j = 0; j < m_; j++) {
            matrix_[i][j] += b.matrix_[i][j];
        }
    }
    return *this;
}

MatrixParent MatrixParent::operator+(const MatrixParent &b) {
    MatrixParent new_matrix = MatrixParent(*this);
    new_matrix += b;
    return new_matrix;
}


MatrixParent &MatrixParent::operator-=(const MatrixParent &b) {
    if (m_ != b.m_ || n_ != b.n_) {
        throw MatrixException("ОШИБКА РАЗМЕРНОСТИ");
    }
    for (int i = 0; i < n_; i++) {
        for (int j = 0; j < m_; j++) {
            matrix_[i][j] -= b.matrix_[i][j];
        }
    }
    return *this;
}

MatrixParent MatrixParent::operator-(const MatrixParent &b) {
    MatrixParent new_matrix = MatrixParent(*this);
    new_matrix -= b;
    return new_matrix;
}

MatrixParent MatrixParent::operator-() {
    MatrixParent new_matrix = MatrixParent(*this);
    new_matrix.n_ = n_;
    new_matrix.m_ = m_;
    for (int i = 0; i < n_; i++) {
        for (int j = 0; j < m_; j++) {
            new_matrix.matrix_[i][j] = -matrix_[i][j];
        }
    }

    return new_matrix;
}

std::ostream &operator<<(std::ostream &os, const MatrixParent &a) {
    for (int i = 0; i < a.n_; i++) {
        for (int j = 0; j < a.m_; j++) {
            os << a.matrix_[i][j] << ' ';
        }
        os << std::endl;
    }
    return os;
}

MatrixParent MatrixParent::identity(int m) {
    if (m <= 0) {
        throw MatrixException("index <= 0");
    }
    MatrixParent a(m, m);
    for (int i = 0; i < m; i++) {
        a.matrix_[i][i] = 1;
    }

    return a;
}

MatrixParent MatrixParent::diagonal(const double *vals, int m) {
    if (m <= 0) {
        throw MatrixException("index <= 0");
    }
    MatrixParent a(m, m);
    for (int i = 0; i < m; i++) {
        a.matrix_[i][i] = vals[i];
    }

    return a;
}

int MatrixParent::rows() const {
    return n_;
}

int MatrixParent::columns() const {
    return m_;
}

void MatrixParent::set(int n, int m, double val) const {
    if (n < n_ && m < m_) {
        matrix_[n][m] = val;
    } else {
        throw MatrixException("ошибка доступа к полю матрицы");
    }
}

